* Note: this code is no longer maintained since 2014. It was last tested on Hadoop2.4.1(multi-machine cluster)/Hadoop2.7.7(single-machine cluster).

* Run instructions

1. Config parameters: update DataXRay settings at src/harness/Params.java
goodFeatureErrorBound <early accept, errorrate> default; 0.9
goodFeatureVarianceBound <early accept, variance> default; 0.05
badFeatureErrorBound <early prune, errorrate> default; 0.6
badFeatureVarianceBound <early prune, variance> default; 0.1
alpha <alpha> default; 0.5

2. Export java Runable JAR file: src/harness/Run.java

3. Run DataXRay:
-dir <dir>: HDFS path for intermediate files;
-input <path>: input file path, stored in HDFS.