package base;

import java.util.ArrayList;

/* Define strcuture vector */
public class StructureVector extends ArrayList<Integer>{

	/* Initialize */
	public StructureVector(){
		super();
	}
	/* Initialize from string */
	public StructureVector(String line){		
		// initialize feature vector from a line
		String[] dimensions = line.split(":");
		for(String dim:dimensions)
			this.add(Integer.valueOf(dim));
	}
	
	/* get accumulated levels */
	public int getLevel(){
		int level = 0;
		for(int val:this)
			level += val;
		return level;
	}
	
	/* convert into string */
	public String toString(){
		StringBuilder result = new StringBuilder();
		for(int val:this)
			result.append(String.valueOf(val) + ":");			
		return result.toString();
	}
}
