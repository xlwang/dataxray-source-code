package base;

import java.util.ArrayList;

/* Define Element */
public class Element {
	public int ID; // element ID
	public boolean value; // element value: true or false
	// public ArrayList<ArrayList<String>> feature_info; // element feature info
	public String[][] feature_info;

	/* initialize element from string */
	public Element(String info) {
		// 1%true%all,l0f1,l1f2...:all,l0f4,l1f5,...:,...:...
		String[] pieces = info.split("%");
		// initialize ID
		ID = Integer.valueOf(pieces[0]);
		// initialize value
		value = Boolean.valueOf(pieces[1]);
		// initialize feature info
		ArrayList<ArrayList<String>> temp_feature_info = new ArrayList<ArrayList<String>>();

		// update feature info
		if (pieces.length > 2) {
			String[] dimensions = pieces[2].split(":");
			int level = 0;
			for (int i = 0; i < dimensions.length; ++i) {
				ArrayList<String> dim = new ArrayList<String>();
				String[] levels = dimensions[i].split("_");
				level = level < levels.length ? levels.length : level;
				for (String f : levels)
					dim.add(f);
				temp_feature_info.add(dim);
			}
			// convert temp_feature_info
			feature_info = new String[dimensions.length][level];
			for (int i = 0; i < dimensions.length; ++i) {
				for (int j = 0; j < level; ++j) {
					try {
						String value = temp_feature_info.get(i).get(j);
						feature_info[i][j] = value;
					} catch (Exception e) {
						feature_info[i][j] = "-1";
					}
				}
			}
		} else
			feature_info = new String[0][0];
	}

	/* get feature vector given a structure vector */
	public FeatureVector getFeatureVector(StructureVector structureVector) {

		// initialize feature vector
		FeatureVector featureVector = new FeatureVector();

		// update feature vector
		for (int i = 0; i < structureVector.size(); ++i) {
			featureVector.add(feature_info[i][structureVector.get(i)]);
		}
		return featureVector;
	}

	/* convert current element into string */
	public String toString() {
		String result = "";
		result += String.valueOf(ID) + "%" + String.valueOf(value) + "%"; // write element ID and value
		for (int i = 0; i < feature_info.length; ++i) {
			for (int j = 0; j < feature_info[0].length; ++j) {
				result += feature_info[i][j] + "_";
			}
			result += ":";
		}
		return result;
	}
}
