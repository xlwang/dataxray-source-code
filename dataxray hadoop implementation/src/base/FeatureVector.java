package base;

import java.util.ArrayList;

/* Define feature vector object */
public class FeatureVector extends ArrayList<String>{
	
	/* Initialize */
	public FeatureVector(){
		super();
	}
	
	/* Initialize from string */
	public FeatureVector(String line){		
		// initialize feature vector from a line
		String[] dimensions = line.split(":");
		for(String dim:dimensions)
			this.add(dim);
	}
	
	/* convert current feature into string */
	public String toString(){
		StringBuilder result = new StringBuilder();
		for(String str:this)
			result.append(str + ":");			
		return result.toString();
	}

}
