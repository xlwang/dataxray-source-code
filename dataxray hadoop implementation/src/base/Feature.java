package base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import harness.Params;

public class Feature {
	// public static final double alpha = 0.5; //fixed alpha for now

	public FeatureVector feature_vector; // feature vector
	public StructureVector structure_vector; // structure vector
	public HashMap<Integer, Element> list_of_elements; // list of elements
	public HashSet<Integer> list_of_elementindex;
	public HashSet<Integer> list_of_parts;
	public int tcount, fcount;

	// properties
	public int[] maxlevel; // maximum level for current feature
	public double errorrate, cost; // error rate and cost
	public boolean is_select; // is_select flag: whether a feature is a descendant feature of already selected
								// feature

	/* Initialize feature */
	public Feature() {
		list_of_elements = new HashMap<Integer, Element>();
		list_of_elementindex = new HashSet<Integer>();
		list_of_parts = new HashSet<Integer>();
	}

	/* Initialize feature with structure vector, feature vector, and max level */
	public Feature(StructureVector s, FeatureVector f, int[] m) {
		structure_vector = s;
		feature_vector = f;
		maxlevel = m;
		list_of_elements = new HashMap<Integer, Element>();
		list_of_elementindex = new HashSet<Integer>();
		list_of_parts = new HashSet<Integer>();
	}

	/* Initialize feature by a string */
	public Feature(String line) {
		list_of_elementindex = new HashSet<Integer>();
		list_of_parts = new HashSet<Integer>();
		String[] properties = line.split(";"); // split properties
		// update max level
		String[] maxs = properties[0].split(":");
		maxlevel = new int[maxs.length];
		for (int i = 0; i < maxs.length; ++i)
			maxlevel[i] = Integer.valueOf(maxs[i]);

		errorrate = Double.valueOf(properties[1]); // update error rate
		cost = Double.valueOf(properties[2]); // update cost
		is_select = Boolean.valueOf(properties[3]); // update is_select flag

		feature_vector = new FeatureVector(properties[4]); // update feature vector
		structure_vector = new StructureVector(properties[5]); // update structure vector

		// get parts
		if (properties.length > 6 && properties[6] != null && properties[6].length() > 0) {
			String[] parts = properties[6].split(":");
			for (String part : parts)
				list_of_parts.add(Integer.valueOf(part));

		}
		// update elements
		list_of_elements = new HashMap<Integer, Element>();
		if (properties.length > 7) {
			String[] elementstr = properties[7].split("=");
			for (int i = 0; i < elementstr.length; ++i) {
				Element element = new Element(elementstr[i]);
				list_of_elements.put(element.ID, element);
			}
		}
		tcount = 0;
		fcount = 0;
	}

	/* get feature overall level */
	public int getLevel() {
		return structure_vector.getLevel();
	}

	/* calculate error rate and cost */
	public void calculateEC() {

		// when there is not elements
		if (this.list_of_elements.size() == 0) {
			this.errorrate = 0;
			this.cost = 0;
			return;
		}

		// get true element count and false element count
		int tcount = 0;
		int fcount = 0;
		for (Element e : this.list_of_elements.values()) {
			if (e.value)
				tcount++;
			else
				fcount++;
		}

		// calculate error rate
		this.errorrate = (double) (fcount) / (fcount + tcount);

		// calculate cost
		if (errorrate > 0 && errorrate < 1)
			this.cost = Math.log(1 / Params.alpha) / Math.log(2) + fcount * Math.log(1 / errorrate) / Math.log(2)
					+ tcount * Math.log(1 / (1 - errorrate)) / Math.log(2);
		else
			this.cost = this.errorrate == 0 ? 0 : Math.log(1 / Params.alpha) / Math.log(2);

	}

	/* calculate error rate and cost */
	public void calculateEC(int fcount, int tcount) {

		if (tcount + fcount > 0)
			errorrate = (double) fcount / (tcount + fcount);
		else
			errorrate = 0;

		// calculate cost
		if (errorrate > 0 && errorrate < 1)
			this.cost = Math.log(1 / Params.alpha) / Math.log(2) + fcount * Math.log(1 / errorrate) / Math.log(2)
					+ tcount * Math.log(1 / (1 - errorrate)) / Math.log(2);
		else
			this.cost = this.errorrate == 0 ? 0 : Math.log(1 / Params.alpha) / Math.log(2);

	}

	/* get a list of parent feature vectors */
	public ArrayList<String> parentFeatureGenerator() {
		// initialize result
		ArrayList<String> result = new ArrayList<String>();
		Element sampleelement = null;
		// choose one element
		for (Integer key : list_of_elements.keySet()) {
			sampleelement = list_of_elements.get(key);
			break;
		}
		if (sampleelement == null)
			return result;
		// = list_of_elements.values().iterator().next();

		// get parenet feature in all dimensions
		for (int i = 0; i < structure_vector.size(); ++i) {

			// must above the first level in current feature dimension;
			// otherwise, no parent feature in this dimension
			if (structure_vector.get(i) > 0) {

				// update structure vector
				structure_vector.set(i, structure_vector.get(i) - 1);

				// get feature vector
				FeatureVector featureVector = sampleelement.getFeatureVector(structure_vector);

				String re = "R2;" + String.valueOf(i) + ";" + featureVector.toString();
				// update result
				result.add(re);

				// update structure vector
				structure_vector.set(i, structure_vector.get(i) + 1);
			}
		}
		// return
		return result;
	}

	/* get a list of child features */
	public ArrayList<StructureVector> nextFeatureGenerator() {
		// initialize result
		ArrayList<StructureVector> result = new ArrayList<StructureVector>();

		// get features
		for (int i = 0; i < structure_vector.size(); ++i) {
			if (structure_vector.get(i) < maxlevel[i]) {

				// update structure vector
				structure_vector.set(i, structure_vector.get(i) + 1);

				result.add((StructureVector) structure_vector.clone());

				// update structure vector
				structure_vector.set(i, structure_vector.get(i) - 1);
			}
		}
		return result;
	}

	/* convert current feature to string */
	public String toString() {
		String out = "";
		for (int max : maxlevel)
			out += String.valueOf(max) + ":";
		out += ";" + String.valueOf(errorrate) + ";" + String.valueOf(cost) + ";" + String.valueOf(is_select) + ";"
				+ feature_vector.toString() + ";" + structure_vector.toString() + ";";
		for (int part : this.list_of_parts)
			out += part + ":";
		out += ";";
		for (Element element : list_of_elements.values()) {
			out += element.toString() + "=";
		}
		return out;
	}

	/* return accumulated max level */
	public int getMaxLevel() {
		int result = 0;
		for (int val : maxlevel)
			result += val;
		return result;
	}

	public void copyFields(Feature feature) {
		this.is_select = feature.is_select;
		this.cost = feature.cost;
		this.errorrate = feature.errorrate;
		this.feature_vector = feature.feature_vector;
		this.structure_vector = feature.structure_vector;
		this.list_of_parts = feature.list_of_parts;
		this.maxlevel = feature.maxlevel;
	}
}
