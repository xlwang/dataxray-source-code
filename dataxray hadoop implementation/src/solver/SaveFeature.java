package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import base.Element;
import base.Feature;

public class SaveFeature {
	public static final int filesize = 10000;

	/* define counter: whether the iterative job should be continued or not */
	public enum ContinueCounter {
		TOCONTINUE
	}

	/* MAPPER: collect feature info */
	public static class Map extends Mapper<LongWritable, Text, Text, Writable> {

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			// merged to previous step reducer
			String[] key_value = value.toString().split("\t");
			String[] pieces = key_value[0].split(";");

			// valid key value must start with "R3"
			if (!pieces[0].equals("R4"))
				return;
			String fname = ""; // define feature name
			for (int i = 0; i < 4; ++i)
				fname += pieces[i] + ";"; // update info

			context.write(new Text(fname), new Text(pieces[4] + "," + key_value[1])); // send to reducer
		}
	}

	/*
	 * REDUCER: get feature name decide how to proceed the feature: selected into
	 * result set; or continue for next round; or pruned
	 */
	public static class Reduce extends Reducer<Text, Writable, Text, Writable> {

		private MultipleOutputs mos; // define multiple Output object

		/* Initialize multiple output object */
		public void setup(Context context) {
			mos = new MultipleOutputs(context);
		}

		// define reduce function
		public void reduce(Text key, Iterable<Writable> STcol, Context context)
				throws IOException, InterruptedException {
			// check input key type
			String[] pieces = key.toString().split(";");

			String fname = pieces[1]; // get feature name (feature vector string)
			String type = pieces[2]; // get feature type: "C" as a child feature or "P" as a parent feature
			int part = Integer.valueOf(pieces[3]);

			Feature current = new Feature(); // define current feature

			// define flags: three feature pools:
			// "S" as selected feature: save parent feature in result set or child feature
			// for next round;
			// "U" as pruned feature;
			// "T" as descendant feature from previous select
			boolean inS = true, inN = true;
			HashMap<Integer, Element> list_of_elements = new HashMap<Integer, Element>();
			while (STcol.iterator().hasNext()) {
				// define value set
				String[] info = STcol.iterator().next().toString().split(",");
				Feature tempfeature = new Feature(info[1]);
				// update current feature
				inN &= info[0].equals("N");
				if (info[0].equals("N")) {
					list_of_elements.putAll(tempfeature.list_of_elements);
				} else {
					inS &= info[0].equals("S");
					current.copyFields(tempfeature);
				}
			}
			current.list_of_elements = list_of_elements;
			// check number of elements
			if (inN || (type.equals("C") && current.list_of_elements.size() == 0))
				return;
			// with full feature info
			if (type.equals("P") && inS) {
				mos.write("result", new Text(current.feature_vector.toString()),
						new Text(current.structure_vector + ";" + String.valueOf(current.errorrate) + ";"
								+ String.valueOf(current.cost) + ";" + String.valueOf(current.list_of_elements.size())),
						"result/current"); // salve to result
			} else if (type.equals("C") && inS) {
				current.is_select = false;
				mos.write("split", new Text(current.feature_vector.toString() + ";" + part),
						new Text(current.toString()), "input/next");
			} else if (type.equals("C") && !inS) {
				current.is_select = true;
				mos.write("split", new Text(current.feature_vector.toString() + ";" + part),
						new Text(current.toString()), "input/next");
			}

		}

		/* clean up for mutliple output object */
		public void cleanup(Context context) throws IOException, InterruptedException {
			mos.close();
		}
	}
}
