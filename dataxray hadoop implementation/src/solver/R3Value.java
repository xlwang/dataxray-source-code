package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import base.Feature;

/** R3Value object 
 *  Function: used for merge feature job */
public class R3Value {
	public Feature current; // current feature
	public List<Feature> childcol; // child features
	public String flagSUT; // flag: S or U or T
	
	/* Initialize the current feature */
	public R3Value(){
		childcol = new ArrayList<Feature>();
	}
	
	/* Initialize the current feature */
	public R3Value(Feature cur, List<Feature> c, String f){
		current = cur;
		childcol = c;
		flagSUT = f;
	}
	
	
	/* Initialize the current feature with a string*/
	public R3Value(String in) throws IOException {
		
		childcol = new ArrayList<Feature>(); // initial child feature col
		String[] pieces = in.split(",");
		flagSUT = pieces[0]; // update flag
		
		// update current feature info
		if(pieces[1].equals("null"))
			current = null;
		else
			current = new Feature(pieces[1]);
		
		// update child feature info
		if(pieces[2].equals("null"))
			childcol = null;
		else{
			String[] children = pieces[2].split("-");
			for(int i = 0; i < children.length; ++i)
				childcol.add(new Feature(children[i]));
		}
	}
	
	/* Convert it in string */
	public String toString() {
		// write to file\
		String out = "";
		out += flagSUT + "," ; // convert flag
		if(current == null) // convert current feature
			out += "null" + ",";
		else
			out += current.toString() + ",";
		if(childcol == null || childcol.size() == 0) // convert child features
			out += "null";
		else for(Feature child:childcol)
				out += child.toString() + "-";
		return out; // return output
	}
	
}
