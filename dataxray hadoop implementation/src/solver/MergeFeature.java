package solver;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import base.*;

/**
 * Merge Feature (3rd step) job: Function: 1. read output from previous job,
 * generate feature and related info (mapper) 2. decide whether a feature should
 * be include in result; or send to next round. (reducer) 3. determine whether
 * all elements are included or not by updating counter. (reducer)
 */
public class MergeFeature {
	public static final int filesize = 10000;

	/* define counter: whether the iterative job should be continued or not */
	public enum ContinueCounter {
		TOCONTINUE
	}

	/* MAPPER: collect feature info */
	public static class Map extends Mapper<LongWritable, Text, Text, Writable> {

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			// merged to previous step reducer
			String[] key_value = value.toString().split("	");
			String[] pieces = key_value[0].split(";");

			// valid key value must start with "R3"
			if (!pieces[0].equals("R3"))
				return;
			String fname = ""; // define feature name
			for (int i = 0; i < 4; ++i)
				fname += pieces[i] + ";"; // update info

			context.write(new Text(fname), new Text(key_value[1])); // send to reducer
		}
	}

	/*
	 * REDUCER: get feature name decide how to proceed the feature: selected into
	 * result set; or continue for next round; or pruned
	 */
	public static class Reduce extends Reducer<Text, Writable, Text, Writable> {

		// define reduce function
		public void reduce(Text key, Iterable<Writable> SUcol, Context context)
				throws IOException, InterruptedException {
			// check input key type
			String[] pieces = key.toString().split(";");

			String type = pieces[2]; // get feature type: "C" as a child feature or "P" as a parent feature
			Feature current = null; // define current feature
			ArrayList<Feature> childcol = new ArrayList<Feature>(); // initialize child feature collection, if any for
																	// parent feature only

			// define flags: three feature pools:
			// "S" as selected feature: save parent feature in result set or child feature
			// for next round;
			// "U" as pruned feature;
			// "T" as descendant feature from previous select
			boolean inS = true, inT = false;
			while (SUcol.iterator().hasNext()) {
				// define value set
				R3Value value = new R3Value(SUcol.iterator().next().toString());
				// update current feature
				current = value.current;
				if (value.childcol != null)
					childcol.addAll(value.childcol); // add child feature
				inS &= value.flagSUT.equals("S"); // update flags
				inT |= value.flagSUT.equals("T");
			}

			if (inS && !inT) {
				// if the current feature is in S feature pool
				if (type.equals("P")) {
					// write parent feature in file
					context.write(new Text("R4;" + current.feature_vector.toString() + ";P;0;S"),
							new Text(current.toString())); // salve to result
					// process child features
					for (Feature child : childcol) {
						child.is_select = true; // update is_select flag
						for (int part : child.list_of_parts)
							context.write(new Text("R4;" + child.feature_vector.toString() + ";C;" + part + ";T"),
									new Text(child.toString()));
					}
				} else {
					// process child feature
					if (current.errorrate == 0) // skip feature with 0 error rate
						return;
					if (current.getLevel() == current.getMaxLevel()) // reach the maximum level
						context.write(new Text("R4;" + current.feature_vector.toString() + ";P;0;S"),
								new Text(current.toString()));
					else {
						for (int part : current.list_of_parts)
							context.write(new Text("R4;" + current.feature_vector.toString() + ";C;" + part + ";S"),
									new Text(current.toString()));
						context.getCounter(ContinueCounter.TOCONTINUE).increment(1); // update counter
					}
				}
			} else if (inT) {
				// if the current feature is in T feature pool
				context.write(new Text("R4;" + current.feature_vector.toString() + ";C;" + pieces[3] + ";T"),
						new Text(current.toString()));

			}
			// ignore feature in U feature pool.
		}

	}
}
