package solver;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.*;

/**
 * Configure the iterative jobs Each round has three jobs: Split Feature;
 * Compare Feature; Merge Feature
 */
public class DataXRay extends Configured implements Tool {

	public int run(String[] args) throws Exception {
		String pdir = args[0];
		boolean start = true;
		long counter = 0;
		int round = 0;
		// iteratively processing
		while (start || counter > 0) {
			start = false;
			Configuration conf = new Configuration(); // set configuration
			FileSystem fs = FileSystem.get(conf); // set file system

			/* Define input/output paths for *three hadoop jobs* in one single round. */
			// Split Feature(1st step) input path; Initialized with input file with one
			// feature(e.g. all:all:...) that includes all elements.
			Path splitinput = new Path(pdir + "split/" + String.valueOf(round) + "/input");
			// Compare Feature (2nd step) input path; Split Feature(1st step) output path;
			Path compareinput = new Path(pdir + "compare");
			// Merge Feature(3rd step) input path; Compare Feature(2nd step) output path;
			Path mergeinput = new Path(pdir + "merge");
			// Save Feature(4th step) input path;
			Path saveinput = new Path(pdir + "save");
			// Merge Feature(3rd step) output path;
			Path splitinputnext = new Path(pdir + "split/" + String.valueOf(++round));

			/* define Split Feature (1st step) hadoop job */
			
			Job splitjob = Job.getInstance();
			splitjob.setJobName("Split Feature"); // set job name
			splitjob.setJarByClass(SplitFeature.class);
			splitjob.setMapperClass(SplitFeature.Map.class); // set mapper class
			// splitjob.setCombinerClass(SplitFeature.Reduce.class); // set combiner class
			splitjob.setReducerClass(SplitFeature.Reduce.class); // set reducer class

			if (fs.exists(compareinput)) // check output path: delete if already exists
				fs.delete(compareinput, true);

			splitjob.setInputFormatClass(TextInputFormat.class); // set job input format
			splitjob.setMapOutputKeyClass(Text.class); // set map output format (key)
			splitjob.setMapOutputValueClass(Text.class); // set map output format (value)
			splitjob.setOutputFormatClass(TextOutputFormat.class); // set job output format

			FileInputFormat.addInputPath(splitjob, splitinput); // set input file path
			String fileinput = "";
			FileStatus[] status = fs.listStatus(splitinput);
			for (int i = 0; i < status.length; ++i) {
				fileinput += status[i].getPath() + ",";
			}

			FileOutputFormat.setOutputPath(splitjob, compareinput); // set output file path

			splitjob.waitForCompletion(true); // run split job

			/* define Compare Feature (2nd step) hadoop job */
			
			Job comparejob = Job.getInstance();
			comparejob.setJobName("Compare Feature"); // set job name
			comparejob.setJarByClass(CompareFeature.class);
			comparejob.setMapperClass(CompareFeature.Map.class); // set mapper
			comparejob.setReducerClass(CompareFeature.Reduce.class); // set reducer

			if (fs.exists(mergeinput)) // check output path
				fs.delete(mergeinput, true);

			comparejob.setInputFormatClass(TextInputFormat.class); // set input format
			comparejob.setMapOutputKeyClass(Text.class); // set mapper output format
			comparejob.setMapOutputValueClass(Text.class); // set mapper output format
			comparejob.setOutputFormatClass(TextOutputFormat.class); // set output format

			FileInputFormat.addInputPath(comparejob, compareinput); // set input path
			FileOutputFormat.setOutputPath(comparejob, mergeinput); // set output path

			comparejob.waitForCompletion(true); // run compare job

			/* define Merge Feature (3rd step) hadoop job */

			Job mergejob = Job.getInstance();
			mergejob.setJobName("Merge Feature"); // set job name
			mergejob.setJarByClass(MergeFeature.class);
			mergejob.setMapperClass(MergeFeature.Map.class); // set mapper
			mergejob.setReducerClass(MergeFeature.Reduce.class); // set reducer

			if (fs.exists(saveinput))
				fs.delete(saveinput, true);

			if (fs.exists(splitinputnext)) // check output path
				fs.delete(splitinputnext, true);

			mergejob.setInputFormatClass(TextInputFormat.class); // set input format
			mergejob.setMapOutputKeyClass(Text.class); // set mapper output format
			mergejob.setMapOutputValueClass(Text.class); // set mapper output format
			mergejob.setOutputFormatClass(TextOutputFormat.class);
			
			FileInputFormat.addInputPath(mergejob, compareinput); // set input path from 1st job
			FileInputFormat.addInputPath(mergejob, mergeinput); // set input path from 2nd job
			FileOutputFormat.setOutputPath(mergejob, saveinput); // set output path
			
			mergejob.waitForCompletion(true); // run merge job

			counter = mergejob.getCounters().findCounter(MergeFeature.ContinueCounter.TOCONTINUE).getValue(); // update
			
			/* save intermediate results */																						// value

			Job savejob = Job.getInstance();
			savejob.setJobName("Save Feature");
			savejob.setJarByClass(SaveFeature.class);
			savejob.setMapperClass(SaveFeature.Map.class);
			savejob.setReducerClass(SaveFeature.Reduce.class);

			savejob.setInputFormatClass(TextInputFormat.class);
			savejob.setMapOutputKeyClass(Text.class);
			savejob.setMapOutputValueClass(Text.class);
			MultipleOutputs.addNamedOutput(savejob, "result", TextOutputFormat.class, Text.class, Text.class);
			MultipleOutputs.addNamedOutput(savejob, "split", TextOutputFormat.class, Text.class, Text.class);

			FileInputFormat.addInputPath(savejob, compareinput);
			FileInputFormat.addInputPath(savejob, saveinput);
			FileOutputFormat.setOutputPath(savejob, splitinputnext);

			savejob.waitForCompletion(true);

		}
		return 0; // finish job
	}
}
