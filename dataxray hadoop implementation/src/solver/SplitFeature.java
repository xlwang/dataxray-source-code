package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import base.*;

/**
 * Split Feature (1st step) job: Function: 1. split parent feature: find child
 * features in next level by structure vector and elements (mapper) 2. calculate
 * error rate and cost for child features (reducer) 3. find partitions for child
 * features (reducer)
 */
public class SplitFeature {
	public static final int filesize = 10000;
	public static final int mapsize = 10000;

	// define the value set for reducer

	public static class Value {
		boolean is_select; // whether this element is covered by selected feature or not
		ArrayList<Element> elements; // the element

		/* initialize */

		public Value(Element e, boolean i) {
			elements = new ArrayList<Element>();
			elements.add(e);
			is_select = i;
		}

		public void add(Element e) {
			elements.add(e);
		}

		/* initialize from string */
		public Value(String line) {
			elements = new ArrayList<Element>();
			String pieces[] = line.split(";");
			is_select = Boolean.valueOf(pieces[0]);
			String[] elementstr = pieces[1].split("=");
			for (int i = 0; i < elementstr.length; ++i)
				elements.add(new Element(elementstr[i]));
		}

		/* convert into string */
		public String toString() {
			String out = String.valueOf(is_select) + ";";
			for (int i = 0; i < elements.size(); ++i)
				out += elements.get(i).toString() + "=";
			return out;
		}

	}

	/* MAPPER: spread element information from a given parent feature */

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {

		public void map(LongWritable k, Text parentstr, Context context) throws IOException, InterruptedException {
			String[] key_value = parentstr.toString().split("	");

			Feature parent = new Feature(key_value[1]);
			String max = "";
			for (int i = 0; i < parent.maxlevel.length; ++i)
				max += String.valueOf(parent.maxlevel[i]) + ":";

			// get all structure vectors in next level
			ArrayList<StructureVector> structurevectors = parent.nextFeatureGenerator();
			for (StructureVector structurevector : structurevectors) {
				// for every structure vector
				HashMap<String, Value> childlist = new HashMap<String, Value>();
				for (Integer key : parent.list_of_elements.keySet()) {
					// for every element
					Element element = parent.list_of_elements.get(key);
					String childname = element.getFeatureVector(structurevector).toString();
					int part = element.ID / filesize;
					String Rkey0 = "R1;" + childname + ";" + structurevector.toString() + ";" + max + ";" + part;

					if (childlist.containsKey(Rkey0)) {
						childlist.get(Rkey0).add(element);
						if (childlist.get(Rkey0).elements.size() > mapsize) {
							context.write(new Text(Rkey0), new Text(childlist.get(Rkey0).toString()));
							childlist.remove(Rkey0);
						}
					} else {
						Value value = new Value(element, parent.is_select);
						childlist.put(Rkey0, value);
					}
				}
				// print output
				for (String Rkey : childlist.keySet()) {
					context.write(new Text(Rkey), new Text(childlist.get(Rkey).toString()));
				}
			}
			// emit information of parent feature to reducer in step 2
			parent.list_of_elements.clear();
			if (!parent.is_select)
				for (int i = 0; i < parent.feature_vector.size(); ++i)
					context.write(new Text("R2;" + String.valueOf(i) + ";" + parent.feature_vector.toString() + ";"
							+ parent.feature_vector.toString()), new Text("0,0:0," + parent.toString()));
		}

	}

	/* REDUCER: collect information for child features */
	public static class Reduce extends Reducer<Text, Writable, Text, Writable> {

		public void reduce(Text cname, Iterable<Writable> valuecol, Context context)
				throws IOException, InterruptedException {
			// check key
			String[] pieces = cname.toString().split(";");

			// valid key must start with "R1".
			if (!pieces[0].equals("R1")) {
				// otherwise, directly transfer to next job.
				while (valuecol.iterator().hasNext()) {
					// write into output
					context.write(cname, new Text(valuecol.iterator().next().toString()));
					return;
				}
			}
			// get feature vector
			FeatureVector feature_vector = new FeatureVector(pieces[1]);
			// get structure vector
			StructureVector structure_vector = new StructureVector(pieces[2]);
			// get maximum level info
			String[] maxs = pieces[3].split(":");
			int[] maxlevel = new int[maxs.length];
			for (int i = 0; i < maxs.length; ++i)
				maxlevel[i] = Integer.valueOf(maxs[i]);

			// define new feature
			Feature child = new Feature(structure_vector, feature_vector, maxlevel);

			int tcount = 0, fcount = 0;
			while (valuecol.iterator().hasNext()) {
				// create value object and update child feature info
				SplitFeature.Value value = new SplitFeature.Value(valuecol.iterator().next().toString());
				// boolean is_select = value.is_select; // get is_select flag
				for (int i = 0; i < value.elements.size(); ++i) {
					Element element = value.elements.get(i); // get element
					child.list_of_elements.put(element.ID, element); // add element
					if (element.value)
						tcount++;
					else
						fcount++;

				}
				child.is_select |= value.is_select;
			}

			Feature subfeature = new Feature();
			subfeature.copyFields(child);

			// collect results
			// collect parent feature information
			// send to next hadoop job
			ArrayList<String> partitions = child.parentFeatureGenerator();
			for (String partition : partitions)
				context.write(new Text(partition + ";" + child.feature_vector.toString()),
						new Text(pieces[4] + "," + tcount + ":" + fcount + "," + subfeature.toString())); // to function
																											// in step 2

			if (child.is_select) {
				R3Value value = new R3Value(subfeature, null, "T"); // create value set for function in step 3
				context.write(new Text("R3;" + child.feature_vector.toString() + ";C;" + pieces[4] + ";T"),
						new Text(value.toString())); // to function in step 3
			}

			context.write(new Text("R4;" + child.feature_vector.toString() + ";C;" + pieces[4] + ";N"),
					new Text(child.toString()));
		}
	}

}
