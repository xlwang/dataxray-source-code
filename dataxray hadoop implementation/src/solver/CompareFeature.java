package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import base.Feature;
import harness.Params;

/**
 * Compare Feature (2nd step) job: Function: 1. read output from previous job,
 * generate partition, child feature collection mapping info (mapper) 2. compare
 * parent feature with child features, decide SUTflag (reducer)
 */
public class CompareFeature {
	/* MAPPER: collect partition information */
	public static class Map extends Mapper<LongWritable, Text, Text, Writable> {

		public void map(LongWritable cname, Text child, Context context) throws IOException, InterruptedException {
			// merged to previous step reducer
			String[] key_value = child.toString().split("	");

			String[] pieces = key_value[0].split(";");

			// valid key value must start with "R2"
			if (!pieces[0].equals("R2"))
				return;
			// update partition value
			String partition = "";
			for (int i = 0; i < 3; ++i)
				partition += pieces[i] + ";";
			// proceed to reducer
			context.write(new Text(partition), new Text(key_value[1]));
		}
	}

	/* REDUCER: compare parent feature with child features */
	public static class Reduce extends Reducer<Text, Writable, Text, Writable> {

		public void reduce(Text partition, Iterable<Writable> childcol, Context context)
				throws IOException, InterruptedException {

			// check if parent feature in childcol
			String[] pieces = partition.toString().split(";");

			String pname = pieces[2]; // get parent feature name

			HashMap<String, Feature> childfeatures = new HashMap<String, Feature>(); // initialize child feature
																						// collection
			Feature parent = null; // define parent feature

			// process original child feature collection
			while (childcol.iterator().hasNext()) {
				String[] childstr = childcol.iterator().next().toString().split(","); // get child feature string.
				String part = childstr[0];
				String[] counts = childstr[1].split(":");
				Feature child = new Feature(childstr[2]); // define child feature
				if (pname.equals(child.feature_vector.toString()))
					parent = child;
				else {
					child.tcount = Integer.valueOf(counts[0]);
					child.fcount = Integer.valueOf(counts[1]);
					child.list_of_parts.add(Integer.valueOf(part));
					if (childfeatures.containsKey(child.feature_vector.toString())) {
						Feature current = childfeatures.get(child.feature_vector.toString());
						current.fcount += child.fcount;
						current.tcount += child.tcount;
						current.list_of_parts.addAll(child.list_of_parts);
					} else {
						childfeatures.put(child.feature_vector.toString(), child);
					}
				}
			}
			for (String key : childfeatures.keySet())
				childfeatures.get(key).calculateEC(childfeatures.get(key).fcount, childfeatures.get(key).tcount);

			// check key-value set validation: 1. parent feature must included in original
			// child feature collection; 2. updated child feature collection must have size
			// larger than 0
			if (parent == null || childfeatures.size() == 0)
				return; // otherwise, skip this key-value set

			// calculate variance & cost
			double[] variance_cost = getVarianceCost(childfeatures);

			// determine parentInS
			boolean parentInS;

			if (goodQuality(parent, variance_cost[0]))
				parentInS = true; // parent feature satisfy good quality constraints
			else if (badQuality(parent, variance_cost[0]))
				parentInS = false; // parent featuer satisfy bad quality constraints
			else
				parentInS = (parent.cost < variance_cost[1]); // compare actual cost

			// collect results
			if (parentInS) {
				// if the parent feature is in S set, define value object for parent feature with S flat
				R3Value value = new R3Value(parent, new ArrayList<Feature>(childfeatures.values()), "S"); 
				context.write(new Text("R3;" + parent.feature_vector.toString() + ";P" + ";0; S"),
						new Text(value.toString())); // write output

				// define value objects for child features with U flag
				for (Feature child : childfeatures.values()) {
					R3Value childvalue = new R3Value(child, null, "U");
					for (int part : child.list_of_parts)
						context.write(new Text("R3;" + child.feature_vector.toString() + ";C;" + part + ";U"),
								new Text(childvalue.toString())); // write output
				}
			} else { // if parent feature in U set
				R3Value value = new R3Value(null, null, "U"); // define value object for parent feature with U flag
				context.write(new Text("R3;" + parent.feature_vector.toString() + ";P" + ";0; U"),
						new Text(value.toString())); // write output

				// define value objects for child features with S flag
				for (Feature child : childfeatures.values()) {
					R3Value childvalue = new R3Value(child, null, "S");
					for (int part : child.list_of_parts)
						context.write(new Text("R3;" + child.feature_vector.toString() + ";C;" + part + ";S"),
								new Text(childvalue.toString())); // write output
				}
			}
		}
	}

	/* compute variance and cost given a set of features */
	public static double[] getVarianceCost(HashMap<String, Feature> features) {
		double[] info = new double[2];

		// calculate mean value
		double avg = 0;
		for (Feature f : features.values()) {
			avg += f.errorrate;
		}
		avg = avg / features.size();
		double variance = 0, cost = 0;
		// calculate variance and accumulated cost
		for (Feature f : features.values()) {
			variance += Math.pow(f.errorrate - avg, 2);

			if (!f.is_select) // only include cost when it is not selected yet
				cost += f.cost;
		}
		variance = variance / features.size();

		// define return values
		info[0] = variance;
		info[1] = cost;

		return info;
	}

	/* return true if given feature is a good feature */
	public static boolean goodQuality(Feature f, double variance) {
		if (f.errorrate > Params.goodFeatureErrorBound && variance < Params.goodFeatureVarianceBound)
			return true;
		else
			return false;
	}

	/* return true if given feature is a bad feature */
	public static boolean badQuality(Feature f, double variance) {
		if (f.errorrate < Params.badFeatureErrorBound || variance > Params.badFeatureVarianceBound)
			return true;
		else
			return false;
	}

}
