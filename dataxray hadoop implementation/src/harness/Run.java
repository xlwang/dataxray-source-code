package harness;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.ToolRunner;

import solver.DataXRay;

public class Run {

	public final static String input_template = "%s/split/0/input";
	public final static String output_template = "%s/result";
	
	public final static String helpstr = "HELP MENU: (*mendatory fields*) \n "
			+ "[-dir <path>] \n "
			+ "[-input <path>] \n";

	public static int prepare_file_system(Configuration config, FileSystem fs, String dir, String input) throws IOException {
		String input_path = String.format(input_template, dir);
		fs.create(new Path(input_path),true);
		FileUtil.copy(fs, new Path(input), fs, new Path(input_path), false, config);
		return 0;
	}

	public static void main(String[] arg) throws Exception {
		String dir = null;
		String input = null;
		// handle input parameters
		int i = 0;
		while (i < arg.length) {
			switch (arg[i]) {
			case "-help":
				// display help menu
				System.out.print(helpstr);
				return;
			case "-dir":
				dir = arg[i + 1];
				i = i + 2;
				break;
			case "-input":
				input = arg[i + 1];
				i = i + 2;
				break;
			default:
				i = i + 1;
			}
		}
		input = "/Users/xiaolanwang/Downloads/hadoop-2.7.7/test/input/test0";
		dir = "/Users/xiaolanwang/Downloads/hadoop-2.7.7/test/input4/";
		
		// Setup configurations
		Configuration config = new Configuration();
		FileSystem fs = FileSystem.get(config);
		
		// Create directory for intermediate results
		Run.prepare_file_system(config, fs, dir, input);
		
		// Run DataXRay
		String[] arg_run = { dir };
		ToolRunner.run(new DataXRay(), arg_run);
		
		// Save result
		ResultCollection result = new ResultCollection();
		result.getResults(dir);
		String output_path = String.format(output_template, dir);
		BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(fs.create(new Path(output_path),true)));
		bw.write(result.getBadFeatures());
		bw.close();
	}

}
