package harness;

public class Params {
	// alpha
	public final static double alpha = 0.5;
	// early accept, errorrate
	public final static double goodFeatureErrorBound = 0.9;
	// early accept, variance
	public final static double goodFeatureVarianceBound = 0.05;
	// early prune, errorrate
	public final static double badFeatureErrorBound = 0.6;
	// early prune, variance
	public final static double badFeatureVarianceBound = 0.1;
}
