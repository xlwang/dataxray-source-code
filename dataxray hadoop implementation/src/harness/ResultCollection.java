package harness;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class ResultCollection {
	public class Result {
		String featurevector;
		String structurevector;
		double errorrate;
		double cost;
		int elecount;

		public Result(String line) {
			try {
				String[] pieces = line.split("	");
				featurevector = pieces[0];
				String[] values = pieces[1].split(";");
				structurevector = values[0];
				errorrate = Double.valueOf(values[1]);
				cost = Double.valueOf(values[2]);
				elecount = Integer.valueOf(values[3]);
			} catch (Exception e) {
				featurevector = null;
				structurevector = null;
				errorrate = -1;
				cost = -1;
				elecount = -1;
			}
		}

		public String toString() {
			String out = featurevector + "\t" + structurevector + ";" + String.valueOf(errorrate) + ";"
					+ String.valueOf(cost) + ";" + String.valueOf(elecount);
			return out;
		}

	}

	public HashMap<String, Result> resultmap;

	public ResultCollection() {
		resultmap = new HashMap<String, Result>();
	}

	public String getBadFeatures() {
		String out = "";
		for (String key : resultmap.keySet()) {
			out += resultmap.get(key).toString() + "\n";
		}
		return out;
	}

	public void getResults(String pdir) throws IOException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		// read results
		Path path = new Path(pdir + "bdc/split/");
		FileStatus[] status = fs.listStatus(path); // list all subdirectories: 0; 1; ...
		for (int i = 0; i < status.length; ++i) {
			if (status[i].isDirectory()) {
				// is a directory
				String temp = status[i].getPath().toString() + "/result";
				if (fs.exists(new Path(temp))) {
					// result folder exists
					FileStatus[] resultfiles = fs.listStatus(new Path(temp));
					for (int j = 0; j < resultfiles.length; ++j) {
						if (resultfiles[j].isFile()) {
							BufferedReader br = new BufferedReader(
									new InputStreamReader(fs.open(resultfiles[j].getPath())));
							String line;
							line = br.readLine();
							while (line != null) {
								Result r = new Result(line);
								if (r.featurevector != null)
									resultmap.put(r.featurevector, r);
								line = br.readLine();
							}
						}
					}
				}
			}
		}
	}
}
